export default async function fetchData(url, data) {
  let settings;
  const isPush = data !== undefined && data !== null;
  if (isPush) {
    settings = {
      method: 'POST',
      headers: new Headers(),
      body: JSON.stringify(data)
    }
  }

  console.log(url);

  try {
    const response = isPush ? await fetch(url, settings) : await fetch(url)
    console.log(response);
    if (response.ok) return await response.json()
    return Promise.reject(new Error(response.status + ' ' + response.statusText))
  }
  catch (error) {
    return Promise.reject(error)
  }
}
