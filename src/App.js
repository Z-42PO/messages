import React from 'react';
import Message from './components/Message';
import AddMessage from './components/AddMessage';
// import fetchData from './apiToolBox';
import './App.css';

// fake API json
const json = {
  "messages": [
    { "content": "1. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla laoreet erat eget nunc euismod, et faucibus purus dignissim.", "private": false },
    { "content": "2. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla laoreet erat eget nunc euismod, et faucibus purus dignissim.", "private": true },
    { "content": "3. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla laoreet erat eget nunc euismod, et faucibus purus dignissim.", "private": true },
    { "content": "4. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla laoreet erat eget nunc euismod, et faucibus purus dignissim.", "private": false },
  ]
}

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      messages: [],
      newMessage: {
        content: "",
        private: false
      }
    };
  }

  /**
   * Store the new message in state
   * @param {String} name name of the element to store its value (content or private)
   * @param {Object} ev triggered event
   */
  storeNewMessage = name => ev => {
    let newMessage = this.state.newMessage;
    const value = name === 'content' ? 'value' : 'checked';
    newMessage[name] = ev.target[value];
    this.setState({ newMessage });
  }

  /**
   * Reset the new message stored in in state
   */
  clearStoredNewMessage = () => {
    const newMessage = { content: "", private: false }
    this.setState({ newMessage })
  }

  /**
   * Push the new message in Front (state) and in API
   */
  pushNewMessage = () => {
    let messages = this.state.messages;
    messages.push(this.state.newMessage);
    this.setState({ messages })
    // fetchData('https://...', this.state.newMessage);
    this.clearStoredNewMessage();
  }

  componentDidMount() {
    // fetchData('https://...')
    Promise.resolve(json)
      .then(
        result => {
          this.setState({ isLoaded: true, messages: result.messages });
        },
        error => {
          this.setState({ isLoaded: true, error });
        }
      )
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div>
            {this.state.isLoaded && this.state.messages.map((message, i) => (
              <Message
                key={i}
                id={message.id}
                content={message.content}
                private={message.private}
              />
            ))}
          </div>
          <AddMessage
            newMessage={this.state.newMessage}
            storeNewMessage={this.storeNewMessage}
            pushNewMessage={this.pushNewMessage}
          />
        </header>
      </div>
    );
  }
}