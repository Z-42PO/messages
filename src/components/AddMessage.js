import React from 'react';
import { TextField, Button, FormControlLabel, Switch, Typography } from '@material-ui/core';
import './AddMessage.css';

export default class AddMessage extends React.Component {
  render() {
    return (
      <form className="form" noValidate autoComplete="off">
        <TextField
          className="TextField"
          id="outlined-multiline-static"
          label="Your message"
          multiline
          rows="4"
          variant="outlined"
          value={this.props.newMessage.content}
          onChange={this.props.storeNewMessage('content')}
        />
        <FormControlLabel
          className="FormControlLabel"
          label={
            <Typography>Private ?</Typography>
          }
          control={
            <Switch
              value="privateSwitch"
              checked={this.props.newMessage.private}
              inputProps={{ 'aria-label': 'private checkbox' }}
              onChange={this.props.storeNewMessage('private')}
            />
          }
        />
        <div>
          <Button variant="contained" color="primary" onClick={this.props.pushNewMessage}>
            Add message
          </Button>
        </div>
      </form>
    );
  }
}
