import React from 'react';
import { Card, CardContent, Typography, Chip } from '@material-ui/core';
import './Message.css';

export default class Message extends React.Component {
  render() {
    // set chip color and label
    let chip = { color: "primary", label: "public" };
    if (this.props.private) { chip.color = "secondary"; chip.label = "private" }

    return (
      <div>
        <Card id={this.props.id} className="card">
          <CardContent>
            <Typography variant="h5" component="h2">
              {this.props.content}
            </Typography>
            <Chip color={chip.color} label={chip.label}></Chip>
          </CardContent>
        </Card>
      </div>
    );
  }
}
